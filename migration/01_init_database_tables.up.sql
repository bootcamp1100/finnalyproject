CREATE TABLE cars (
	id CHAR(36) PRIMARY KEY,
	model VARCHAR(255) UNIQUE NOT NULL,
	color VARCHAR(255) NOT NULL,
	price int NOT NULL,
	user_id CHAR(36),
	created_at TIMESTAMP DEFAULT now(),
	updated_at TIMESTAMP,
	deleted_at TIMESTAMP
);

CREATE TABLE users (
	id CHAR(36) PRIMARY KEY,
	first_name VARCHAR(255) NOT NULL,
	last_name VARCHAR(255) NOT NULL,
	age int NOT NULL,
	created_at TIMESTAMP DEFAULT now(),
	updated_at TIMESTAMP,
	deleted_at TIMESTAMP
);

ALTER TABLE cars ADD CONSTRAINT fk_cars_users FOREIGN KEY (user_id) REFERENCES users (id);