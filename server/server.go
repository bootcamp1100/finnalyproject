package main

import (
	"fmt"
	"log"
	"net"
	"uacademy/grpc_example/config"
	"uacademy/grpc_example/proto-gen/user"
	dService "uacademy/grpc_example/server/services/user"
	"uacademy/grpc_example/storage"
	"uacademy/grpc_example/storage/postgres"

	"google.golang.org/grpc"
	"google.golang.org/grpc/reflection"
)

func main() {
	cfg := config.Load()

	var inter storage.StorageInterface
	stringConn := fmt.Sprintf("user=%s password=%s dbname=%s sslmode=disable", cfg.PostgresUser, cfg.PostgresPassword, cfg.PostgresDatabase)

	inter, err := postgres.InitDb(stringConn)
	if err != nil {
		panic(err)
	}

	println("gRPC server tutorial in Go")

	listener, err := net.Listen("tcp", ":9000")
	if err != nil {
		panic(err)
	}

	c := &dService.TutorialService{
		Stg: inter,
	}
	s := grpc.NewServer()
	user.RegisterTutorialServer(s, c)
	reflection.Register(s)

	if err := s.Serve(listener); err != nil {
		log.Fatalf("failed to serve: %v", err)
	}
}
