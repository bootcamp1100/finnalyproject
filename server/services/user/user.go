package dice

import (
	"context"

	"uacademy/grpc_example/proto-gen/user"
	"uacademy/grpc_example/storage"
)

// TutorialService is a struct that implements the server interface
type TutorialService struct {
	user.UnimplementedTutorialServer
	Stg storage.StorageInterface
}

// RollDice ...
func (s *TutorialService) CreateCar(ctx context.Context, req *user.CreateCarRequest) (*user.CreateCarResponse, error) {
	res, err := s.Stg.CreateCar(*req)
	if err != nil {
		panic(err)
	}
	return &res, nil
}

func (s *TutorialService) GetListCar(ctx context.Context, req *user.GetListCarRequest) (*user.GetListCarResponse, error) {
	res, err := s.Stg.GetListCar(*req)
	if err != nil {
		panic(err)
	}
	return &res, nil
}
