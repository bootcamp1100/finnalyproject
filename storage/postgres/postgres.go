package postgres

import (
	"github.com/jmoiron/sqlx"
	_ "github.com/lib/pq"
)

type Postgres struct {
	Db *sqlx.DB
}

func InitDb(stringConn string) (p *Postgres, err error) {
	db, err := sqlx.Connect("postgres", stringConn)
	if err != nil {
		return p, err
	}
	
	return &Postgres{
		Db: db,
	},nil
}
