package postgres

import (
	"uacademy/grpc_example/proto-gen/user"

	"github.com/google/uuid"
)

func (p Postgres) CreateCar(req user.CreateCarRequest) (user.CreateCarResponse, error) {
	id := uuid.New().String()

	_, err := p.Db.Exec("INSERT INTO cars (id, model, color,price,created_at) VALUES ($1, $2, $3,$4,now())", id, req.Model, req.Color, req.Price)
	if err != nil {
		return user.CreateCarResponse{}, err
	}

	return user.CreateCarResponse{
		Id: id,
	}, nil
}

func (p Postgres) GetListCar(req user.GetListCarRequest) (user.GetListCarResponse, error) {
	var res user.GetListCarResponse

	rows, err := p.Db.Queryx("SELECT id, model, color,price,created_at  FROM cars WHERE deleted_at IS NULL")
	if err != nil {
		return res, err
	}

	for rows.Next() {
		var a user.Car

		err := rows.Scan(
			&a.Id,
			&a.Model,
			&a.Color,
			&a.Price,
			&a.CreateAt,
		)

		if err != nil {
			return res, err
		}

		res.Cars = append(res.Cars, &a)
	}

	return res, nil
}
