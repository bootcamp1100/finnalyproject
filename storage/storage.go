package storage

import "uacademy/grpc_example/proto-gen/user"

type StorageInterface interface {
	CreateCar(req user.CreateCarRequest) (user.CreateCarResponse, error)
	GetListCar(req user.GetListCarRequest) (user.GetListCarResponse,error)
}
